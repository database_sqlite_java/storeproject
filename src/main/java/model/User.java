/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author sany
 */
public class User {

    private int id;
    private String name;
    private String tel;
    private String password;

    public User(int id, String name, String tel, String passeord) {
        this.id = id;
        this.name = name;
        this.tel = tel;
        this.password = password;
    }
    
    public User(int id,String name, String tel) {
        this(id, name, tel, "");
    }

    public User(String name, String tel, String password) {
        this(-1, name, tel, password);
    }
    
    public User(String name, String tel) {
        this(-1, name, tel,"");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getPass() {
        return password;
    }

    public void setPass(String pass) {
        this.password = pass;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", name=" + name + ", tel=" + tel + ", pass=" + password + '}';
    }

}
