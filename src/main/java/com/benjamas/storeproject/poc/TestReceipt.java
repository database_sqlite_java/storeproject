/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benjamas.storeproject.poc;

import model.Customer;
import model.Product;
import model.Receipt;
import model.User;

/**
 *
 * @author sany
 */
public class TestReceipt {
    
    public static void main(String[] args) {
        Product p1 = new Product(1,"thai tea",30);
        Product p2 = new Product(2,"green tea",30);
        
        User seller = new User("Ben","0000000000","password");
        Customer customer = new Customer("Ja","0000000000");
        Receipt receipt = new Receipt(seller , customer);
        receipt.addReceiptDetail(p1,1);
        receipt.addReceiptDetail(p2,3);
        System.out.println(receipt);
        
        receipt.deleteReceiptDetail(0);
        System.out.println(receipt);
        
        receipt.addReceiptDetail(p1,2);
        receipt.addReceiptDetail(p2,5);
        System.out.println(receipt);
        
    }
    
}
